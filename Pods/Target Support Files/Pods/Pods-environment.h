
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Alamofire
#define COCOAPODS_POD_AVAILABLE_Alamofire
// This library does not follow semantic-versioning,
// so we were not able to define version macros.
// Please contact the author.
// Version: 1.2.3.1.

// HanekeSwift
#define COCOAPODS_POD_AVAILABLE_HanekeSwift
// This library does not follow semantic-versioning,
// so we were not able to define version macros.
// Please contact the author.
// Version: 0.9.1.1.

// SwiftyJSON
#define COCOAPODS_POD_AVAILABLE_SwiftyJSON
// This library does not follow semantic-versioning,
// so we were not able to define version macros.
// Please contact the author.
// Version: 2.2.0.1.

